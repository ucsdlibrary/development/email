# Email

Email is a Helm chart that leverage the [docker-postfix][docker-postfix] image
to support postfix email requirements of web applications.

## TL;DR;

```console
$ helm repo add ucsd-helm https://lib-helm-repo.ucsd.edu
$ helm install my-release email
```

## Introduction

This chart bootstraps a [docker-postfix][docker-postfix] deployment on a [Kubernetes](http://kubernetes.io) cluster using the [Helm](https://helm.sh) package manager.

## Installing the Chart
To install the chart with the release name `my-release`:

```console
$ helm install my-release email
```

The command deploys email on the Kubernetes cluster in the default configuration. The [Parameters](#parameters) section lists the parameters that can be configured during installation.

> **Tip**: List all releases using `helm list`

## Uninstalling the Chart

To uninstall/delete the `my-release` deployment:

```console
$ helm delete my-release
```

The command removes all the Kubernetes components associated with the chart and deletes the release.

## Parameters

The following tables lists the configurable parameters of the email chart and their default values, in addition to chart-specific options. These map to the docker-postfix image [configuration options][postfix-config].

| Parameter | Description | Default |
| --------- | ----------- | ------- |
| `image.repository` | docker-postfix repository | `boky/postfix` |
| `image.tag` | docker-postfix image tag to use | `latest` |
| `image.pullPolicy` | docker-postfix image pullPolicy | `IfNotPresent` |
| `imagePullSecrets` | Array of pull secrets for the image | `IfNotPresent` |
| `nameOverride` | String to partially override email.fullname template with a string (will prepend the release name) | `nil` |
| `fullnameOverride` | String to fully override email.fullname template | `nil` |
| `persistence.enabled` | Determine whether a PVC will be setup for the `spoolfile` | `true` |
| `persistence.size` | Size of the PVC to allocate | `256Mi` |
| `network_policy` | Determine whether to enable a NetworkPolicy restricting port access. Matches on release name label | `true` |
| `envVars` | [Environment variables][postfix-config] for the postfix container | `{}` (evaluated as a template) |

[docker-postfix]:https://github.com/bokysan/docker-postfix
[postfix-config]:https://github.com/bokysan/docker-postfix#configuration-options
